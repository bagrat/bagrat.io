---
type: "mooc"

# General
name: "Data Science and Engineering With Apache Spark"
link: "https://credentials.edx.org/credentials/1974627ebd724f548772bc4c5cc73934/?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base%3BIugbz2%2FERr2cDWSLPp7IOw%3D%3D"
date: 2016
endDate: 0
weight: 70

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: "edX.org, UC Berkeley"

# Experience
position: ""
location: ""
info: ""
---

