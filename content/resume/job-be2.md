---
type: "job"

# General
name: "be2 LLC"
link: "https://be2.com/"
date: 2012-07-01
endDate: 2013-03-01

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: ""

# Experience
position: "Software Engineer"
location: "Yerevan, Armenia"
info: "(branch of be2 S.a.r.l., Luxembourg)"
---

Maintained RESTful Web Services for a matchmaking web application. Improved software development processes by setting up the development environment and writing automation scripts, which increased development productivity. Increased code coverage with unit tests and documentation which improved code readability and decreased build fails.
