---
type: "mooc"

# General
name: "MongoDB for DBAs"
link: "https://university.mongodb.com/course_completion/b2cfff7dfbbd4aa99da4c275bc315d93"
date: 2015
endDate: 0
weight: 90

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: "MongoDB University"

# Experience
position: ""
location: ""
info: ""
---

