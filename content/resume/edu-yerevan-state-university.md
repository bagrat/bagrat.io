---
type: "edu"

# General
name: "Yerevan State University"
link: "http://ysu.am/faculties/en/Physics"
date: 0
endDate: 2012

# Technologies
techs: []

# Education
degree: "Bachelor of Science"
major: "Physics"

# MOOC
provider: ""

# Experience
position: ""
location: ""
info: ""
---

