---
type: "mooc"

# General
name: "Distributed Machine Learning With Apache Spark"
link: "https://courses.edx.org/certificates/a57b2bf60d7d43ed91af815e76a2f760?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base%3BIugbz2%2FERr2cDWSLPp7IOw%3D%3D"
date: 2016
endDate: 0
weight: 60

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: "edX.org, UC Berkeley"

# Experience
position: ""
location: ""
info: ""
---

