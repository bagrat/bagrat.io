---
type: "job"

# General
name: "Aarki CJSC"
link: "https://www.aarki.com/"
date: 2014-11-01
endDate: 2015-10-01

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: ""

# Experience
position: "Software Engineer"
location: "Yerevan, Armenia"
info: "(branch of Aarki Inc., Mountain View, CA, USA)"
---

Designed scalable web services for an advertising platform, set up the infrastructure, including CI and configuration management. Developed wrappers/decorators for Python to reduce repeating and routine implementation tasks, which increased development and debugging time by providing more intuitive and clean code.
