---
type: "job"

# General
name: "IUnetworks LLC"
link: "https://iunetworks.am/#aboutUS"
date: 2013-03-01
endDate: 2014-11-01

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: ""

# Experience
position: "Software Engineer"
location: "Yerevan, Armenia"
info: ""
---

Designed 3-tier architecture for an e-commerce web service using Java. Organized the CI pipeline, which increased software quality and release processes and decreased development time. Developed base classes for unit testing which eased test coding by initialising and providing all necessary resources. Implemented business logic split into service and data access layers.
