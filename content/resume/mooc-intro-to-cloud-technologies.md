---
type: "mooc"

# General
name: "Intro to Cloud Technologies"
link: "https://courses.edx.org/certificates/ca37a33a9f8640f2a57d23c605885af8?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base%3BIugbz2%2FERr2cDWSLPp7IOw%3D%3D"
date: 2016
endDate: 0
weight: 20

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: "edX.org, Linux Foundation"

# Experience
position: ""
location: ""
info: ""
---

