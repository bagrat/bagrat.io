---
type: "mooc"

# General
name: "Machine Learning"
link: "/static/img/ml-2014.pdf"
date: 2014
endDate: 0
weight: 80

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: "Coursera.org, Stanford University"

# Experience
position: ""
location: ""
info: ""
---

