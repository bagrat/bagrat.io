---
type: "edu"

# General
name: "American University of Armenia"
link: "https://cse.aua.am"
date: 0
endDate: 2015

# Technologies
techs: []

# Education
degree: "Master of Science"
major: "Computer and Information Science"

# MOOC
provider: ""

# Experience
position: ""
location: ""
info: ""
---

