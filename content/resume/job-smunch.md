---
type: "job"

# General
name: "Smunch GmbH"
link: "https://smunch.co"
date: 2020-09-01
endDate: 0

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: ""

# Experience
position: "Software Engineer"
location: "Berlin, Germany"
info: "(working remotely from Yerevan, Armenia)"
---

Maintain existing legacy services of Smunch, gather requirements and develop new tools for internal teams using Node.js.
