---
type: "mooc"

# General
name: "Intro to Apache Spark"
link: "https://courses.edx.org/certificates/194db88ab1114f2a806015769cd694b6?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base%3BIugbz2%2FERr2cDWSLPp7IOw%3D%3D"
date: 2016
endDate: 0
weight: 40

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: "edX.org, UC Berkeley"

# Experience
position: ""
location: ""
info: ""
---

