---
type: "job"

# General
name: "Antel Design LLC"
link: "http://www.knightsbridgeglobal.eu/index.php?option=com_content&view=article&id=3&Itemid=1&lang=en"
date: 2010-02-01
endDate: 2012-07-01

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: ""

# Experience
position: "Embedded Software Engineer"
location: "Yerevan, Armenia"
info: "(acquired by Knightsbridge Group, Vienna, Austria)"
---

Developed a command line interface via RS232 using C language on AVR32 architecture for a proprietary wireless transceiver. Additionally, integrated a third party Wi-Fi module on same platform to enable remote wireless configuration of the radio module. Wrote technical specifications and documentations for each piece of software.
