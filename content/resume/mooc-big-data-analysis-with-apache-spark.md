---
type: "mooc"

# General
name: "Big Data Analysis With Apache Spark"
link: "https://courses.edx.org/certificates/8128c87aae534eadb2d777d63c73f052?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base%3BIugbz2%2FERr2cDWSLPp7IOw%3D%3D"
date: 2016
endDate: 0
weight: 50

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: "edX.org, UC Berkeley"

# Experience
position: ""
location: ""
info: ""
---

