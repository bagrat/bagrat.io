---
type: "job"

# General
name: "Sauce Labs Inc."
link: "https://saucelabs.com"
date: 2015-10-01
endDate: 2020-09-01

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: ""

# Experience
position: "Software Engineer"
location: "San Francisco, CA, USA"
info: "(working remotely from Yerevan, Armenia)"
---

Developed and maintained the core services of Sauce Labs mainly using Go and asynchronous Python, deploying on Kubernetes. Participated in on-call rotation for prompt production troubleshooting. Contributed to the CI/CD pipelines. Implemented caching and rate limiting for the API to unload the database. Set up monitoring of services for better insights into system health, which significantly decreased the response/reaction time to production incidents.
