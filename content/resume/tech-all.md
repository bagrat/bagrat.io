---
name: "All"
type: "tech"
techs:
  - elixir
  - golang
  - python
  - kubernetes
  - helm
  - bash
  - ansible
  - gitlab-ci
  - mongodb
  - mysql
  - redis
---

