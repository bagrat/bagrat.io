---
type: "mooc"

# General
name: "Cloud Computing Applications"
link: "https://www.coursera.org/account/accomplishments/certificate/GY4WW8UGY7"
date: 2015
endDate: 0
weight: 30

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: "Coursera.org, University of Illinois"

# Experience
position: ""
location: ""
info: ""
---

