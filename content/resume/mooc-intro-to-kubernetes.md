---
type: "mooc"

# General
name: "Intro to Kubernetes"
link: "https://courses.edx.org/certificates/13fbb69cbdb441e79bf5800f65cbc18f?lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_view_base%3BIugbz2%2FERr2cDWSLPp7IOw%3D%3D"
date: 2017
endDate: 0
weight: 10

# Technologies
techs: []

# Education
degree: ""
major: ""

# MOOC
provider: "edX.org, Linux Foundation"

# Experience
position: ""
location: ""
info: ""
---

